export class UnitBasedTariffStub {
    constructor(additionalFee) {
        this.additionalFee = additionalFee || 0;
    }

    getAdditionalFee() {
        return this.additionalFee;
    }

    getType() {
        return new UnitBasedTariffTypeStub();
    }
}

export class UnitBasedTariffTypeStub {
    isUnitBased() {
        return true;
    }
}