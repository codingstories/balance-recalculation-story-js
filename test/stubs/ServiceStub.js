import { UnitBasedTariffStub } from './UnitBasedTariffStub';

export class ServiceStub {
    getTariffs() {
        return [new UnitBasedTariffStub()];
    }
}